package com.vp.theatre.domain

import java.math.BigDecimal
import javax.persistence.*

@Entity
data class Seat(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Long,
    @Column(name = "seatRow")
    val row: Char,
    @Column(name = "seatNum")
    val num: Int,
    val price: BigDecimal,
    val description: String
) {
    override fun toString() = "Seat $row - $num $price ($description)"
}