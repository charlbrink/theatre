package com.vp.theatre.repository

import com.vp.theatre.domain.Booking
import com.vp.theatre.domain.Performance
import com.vp.theatre.domain.Seat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SeatRepository: JpaRepository<Seat, Long> {
    fun findByNumAndRow(seatNum: Int, seatRow: Char) : Seat?
}

@Repository
interface PerformanceRepository: JpaRepository<Performance, Long>

@Repository
interface BookingRepository: JpaRepository<Booking, Long> {
    fun findBySeatAndPerformance(seat: Seat, performance: Performance) : Booking?
}