package com.vp.theatre.services

import com.vp.theatre.domain.Seat
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class TheatreService {
    private val hiddenSeats = mutableListOf<Seat>()

    constructor() {
        fun getPrice(rowNumber: Int, seatNumber: Int) : BigDecimal {
            return when {
                rowNumber >= 14 -> BigDecimal(14.50)
                seatNumber <= 2 || seatNumber >= 34 -> BigDecimal(16.50)
                rowNumber == 1 -> BigDecimal(21.00)
                else -> BigDecimal(18.00)
            }
        }

        fun getDescription(rowNumber: Int, seatNumber: Int) : String {
            return when {
                rowNumber == 14 -> "Cheaper seat"
                rowNumber == 15 -> "Back row"
                seatNumber <= 2 || seatNumber >= 34 -> "Restricted view"
                rowNumber == 2 -> "Best View"
                else -> "Standard seat"
            }
        }

        for (rowNumber in 1..15) {
            for (seatNumber in 1..36) {
                hiddenSeats.add(Seat(0, (rowNumber+64).toChar(), seatNumber, getPrice(rowNumber, seatNumber), getDescription(rowNumber, seatNumber)))
            }
        }
    }

    val seats
        get() = hiddenSeats.toList()

    fun find(number: Int, row: Char) : Seat {
        return seats.filter { it.row == row && it.num == number }.first()
    }
}