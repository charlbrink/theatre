package com.vp.theatre.services

import com.vp.theatre.domain.Booking
import com.vp.theatre.domain.Performance
import com.vp.theatre.domain.Seat
import com.vp.theatre.repository.BookingRepository
import com.vp.theatre.repository.SeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookingService() {

    @Autowired
    lateinit var bookingRepository: BookingRepository

    @Autowired
    lateinit var seatRepository: SeatRepository

    fun isSeatFree(seat : Seat, performance: Performance) : Boolean {
        val matchedBookings = findBooking(seat, performance)
        return matchedBookings == null
    }

    fun findSeat(seatNum: Int, seatRow: Char) : Seat? {
        return seatRepository.findByNumAndRow(seatNum, seatRow)
    }

    fun findBooking(seat: Seat, performance: Performance) : Booking? {
        return bookingRepository.findBySeatAndPerformance(seat, performance)
    }

    fun reserveSeat(seat: Seat, performance: Performance, customerName: String) : Booking {
        val booking = Booking(0,customerName)
        booking.seat = seat
        booking.performance = performance

        bookingRepository.save(booking)
        return booking
    }

}